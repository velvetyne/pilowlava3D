# Pilowlava 3D

This repository holds the 3D models of most latin glyphs from the [Pilowlava typeface](https://gitlab.com/StudioTriple/pilowlava). The 3D modeling has been made by Vincent Wagner of [Studio Brot](https://brot.studio/) while the original 2D typeface was created by [Anton Moglia](http://moglia.fr) & [Jérémy Landes](http://studiotriple.fr/). Both the 3D and 2D version are released by the [Velvetyne Type Foundry](http://velvetyne.fr/fonts/pilowlava/).

![specimen1](documentation/red-ish/pilo3d-red.jpg)

Vincent Wagner of [Studio Brot](https://brot.studio/) created a 3D model for a wide selection of latin glyphs of the Pilowlava font! Note that this work is not like a simple automatic extrusion. Vincent carefully sculpted every glyph, achieving the rounder and puffier rendering you could get. Note how the z-thickness of the strokes is not uniform, increasing and decreasing with the width of the strokes, but not linearly. The achieved effect is such as the one you would get by bending a metal tube. This work brings Pilowlava into the haptic world.

Those 3D glyphs have been modeled for subdivision. Each single glyph is made from 76 to 786 polygons. This work is released under the Free Art License, meaning that it's freely usable for any personal or commercial use as long as you credit its author, and modifiable as long as you share the modifications under the same license.

Files are available under the .blend .c4d .fbx and .obj formats.

Contribute or download it on [Velvetyne Type Foundry](http://velvetyne.fr/news/pilowlava-3d/).

Find the Pilowalava 2D sources and fonts on [the dedicated repository](https://gitlab.com/StudioTriple/pilowlava) and on [Velvetyne's website](http://velvetyne.fr/fonts/pilowlava/).

## Specimen

![specimen2](documentation/blue/pilo3d-volcanoes-widerotation.jpg)
![specimen3](documentation/blue/pilo3d-volcanoes.jpg)

## License

Pilowlava 3D is licensed under the the Free Art License, version 1.3.
This license is copied below, and is also available at
https://artlibre.org/

## Repository Layout

This font repository tries to follow the Unified Font Repository v2.0,
a standard way to organize font project source files. Learn more at
https://github.com/unified-font-repository/Unified-Font-Repository
